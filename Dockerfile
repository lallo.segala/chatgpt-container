
FROM ubuntu:22.04

RUN apt-get update -y && apt-get upgrade -y && apt-get install -y wget

WORKDIR /root/

RUN wget -q https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt-get install -y ./google-chrome-stable_current_amd64.deb
RUN apt-get install -y tor

COPY main.sh /root/main.sh

CMD ["sh", "/root/main.sh"]
